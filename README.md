# gulp-json-data-to-sass

Gulp plugin which takes the input of JSON File(s) and outputs SCSS variables. This shoul help you to share Variables between e.g. JS and your CSS.

## Install

``` bash
npm install --save-dev gulp-json-data-to-sass
```

## Quick-Start

``` js
let gulp = require('gulp'),
    jsonToSass = require('gulp-json-data-to-sass');

gulp.task('build:variables', () => {
    return gulp.src('src/conf/**/*.json')
        .pipe(jsonToSass({
            sass: 'src/sass/_variables.scss',
            prefix: '',
            suffix: '',
            separator: '-'
        }));
});
```

## Options

| name		| description																							| type					|
|-----------|-------------------------------------------------------------------------------------------------------|-----------------------|
| sass		| path of SASS file to be generated.																	|  `{String}`			|
| prefix	| Optional prefix to be used for all variables.															|  `{String}`			|
| suffix	| Optional suffix to be used for all variables.															|  `{String}`			|
| separator	| Separator to be used for combining the variable(s), prefix and suffix.								|  `{String}`			|

## JSON Syntax

``` json
{
	"primary": "#EEE",
	"secondary": "#121212",
	"border": {
		"top": "2px",
		"right": "2px",
		"bottom": "0",
		"left": "0"
	}
}
```

This will output:

``` sass
$primary: #EEE;
$secondary: #121212;
$border-top: 2px;
$border-right: 2px;
$border-bottom: 0;
$border-left: 0;
```

## License

Apache-2.0  [Steffen Flohrer](https://bitbucket.org/qm-b/)
