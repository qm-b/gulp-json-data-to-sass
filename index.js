/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const fs = require('fs'),
      PluginError = require('plugin-error'),
      through = require('through2');

const PLUGIN_NAME = 'gulp-json-data-to-sass';

const makeSassVar = (key, options) => {
    scss = '$';
    scss += (options.prefix !== '' ? options.prefix : '') + options.separator;
    scss += key;
    scss += (options.suffix !== '' ? options.separator + options.suffix : '');

    return scss;
};

const isInt = (value) => {
    if (isNaN(value)) {
      return false;
    }
    let x = parseFloat(value);
    return (x | 0) === x;
};

const makeSassObjectItems = (val, indent, options) => {
    let scss = '', prop, ind;
    for(prop in val) {
        if(val.hasOwnProperty(prop)) {
            if(val[prop] && val[prop] instanceof Array) {
                scss += prop + ': (\r\n';
                ind = indent + '\t';
                scss += makeSassListItems(prop, val[prop], options, ind);
                scss += '),\r\n';
            } else if(val[prop] && typeof val[prop] === 'object') {
                scss += makeSassObjectItems(val[prop], indent, options);
            } else {
                scss += indent + prop + ': ';
                scss += val[prop] + ',\r\n';
            }
        }
    }

    return scss;
};

/**
 *
 *
 * @param {String} key
 * @param {Array} arr
 * @param {Object} options
 * @param {String} indent
 * 
 * @returns {String}    SASS List
 */
const makeSassListItems = (key, arr, options, indent) => {
    let scss = '', ind, len = arr.length -1, prop;
    arr.forEach((val, index) => {
        if(val instanceof Array) {
            ind = indent + '\t';
            if(isInt(index)) {
                scss += ' (\r\n';
            } else {
                scss += index + ': (\r\n';
            }
            scss += makeSassListItems(index, val, options, ind);
            scss += ')\r\n';
        } else if(val && typeof val === 'object') {
            ind = indent + '\t';
            if(!isInt(index)) {
                scss += index + ': ';
            }
            scss += '(\r\n';
            scss += makeSassObjectItems(val, indent, options);
            scss += ')\r\n';
        } else {
            scss += indent;
            if(!isInt(index)) {
                scss += index + ': ';
            }
            scss += val + (index === len ? '' : ',');
        }
    });
    return scss;
};

const convert = (jsonData, options, scss) => {
    if (!jsonData) {
        return scss;
    }
    let prop, indent = '\t';
    for (prop in jsonData) {
        if (jsonData.hasOwnProperty(prop)) {
            if(jsonData[prop] && jsonData[prop] instanceof Array) {
                scss += makeSassVar(prop, options);
                scss += ': (\r\n';
                scss += makeSassListItems(prop, jsonData[prop], options, indent);
                scss += ');\r\n';
            } else if(jsonData[prop] && typeof jsonData[prop] === 'object') {
                opts = JSON.parse(JSON.stringify(options));
                opts.prefix = options.prefix + options.separator + prop;
                scss += convert(jsonData[prop], opts, scss);
            } else {
                scss += makeSassVar(prop, options);
                scss += ': ' + jsonData[prop] + ';\r\n';
            }
        }
    }

    return scss;
};

const track = (str) => {
    process.stdout.write(`${PLUGIN_NAME}: ${str}\r\n`);
};

/**
 * Parse JSON String to Object
 * 
 * @param {String} data 
 */
const parse = (data) => {
    
    if(!data) {
        throw new PluginError(PLUGIN_NAME, `${PLUGIN_NAME}: Failed to read file`);
    }
    try {
        data = JSON.parse(data);
    } catch(E) {
        throw new PluginError(PLUGIN_NAME, `${PLUGIN_NAME}: ${E.message}`);
    }
    
    return data;
};

module.exports = (options) => {
    options.json = options.json || 'variables.json';
    options.sass = options.sass || '_variables.scss';
    options.prefix = ( typeof options.prefix === 'string' && options.prefix.length ? options.prefix : '');
    options.suffix = ( typeof options.suffix === 'string' && options.suffix.length ? options.suffix : '');
    options.separator = ( typeof options.separator === 'string' ? options.separator : '-');
    const all = [];
    const transform = (file, enc, next) => {
        
		if (file.isNull()) {
			next(null, file);
			return;
		}

		if (file.isStream()) {
			next(new PluginError(PLUGIN_NAME, `${PLUGIN_NAME}: 'Streaming not supported'`));
            return;
        }
        let data = file.contents.toString('utf8');
        let jsonData = parse(data);
        let scss = convert(jsonData, options, '');
        all.push(scss);
        next(null, scss);
    };
    const end = (cb) => {
        cb();
    };

    const stream = through.obj({ objectMode: true, allowHalfOpen: false }, transform, end)
                .on('finish', function () {
                    let scss = all.join('\n');
                    fs.writeFile(options.sass, scss, 'utf8', (error) => {
                        if(error) {
                            throw new PluginError(PLUGIN_NAME, error);
                        }
                        track('file '+ options.sass +' created successfully');
                        track(scss);
                    });
                });
    
    return stream;
};
