/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const gulp = require('gulp'),
    fs = require('fs'),
    bump = require('gulp-bump');

// Update Major Version:
gulp.task('bump:major', () => {
    return gulp.src(['./package.json'])
        .pipe(bump({type:'major'}))
        .pipe(gulp.dest('./'));
});

// Update Minor Version:
gulp.task('bump:minor', () => {
    return gulp.src(['./package.json'])
        .pipe(bump({type:'minor'}))
        .pipe(gulp.dest('./'));
});

// Update Patch Version:
gulp.task('bump:patch', () => {
    return gulp.src(['./package.json'])
        .pipe(bump({type:'patch'}))
        .pipe(gulp.dest('./'));
});
